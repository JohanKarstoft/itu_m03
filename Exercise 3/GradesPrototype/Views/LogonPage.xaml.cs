﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;

namespace GradesPrototype.Views
{
    /// <summary>
    /// Interaction logic for LogonPage.xaml
    /// </summary>
    public partial class LogonPage : UserControl
    {
        public LogonPage()
        {
            InitializeComponent();
        }

        #region Event Members
        public event EventHandler LogonSuccess;

        //  Exercise 3: Task 1a: Define LogonFailed event

        public event EventHandler logonFailed;

        #endregion

        #region Logon Validation

        // Exercise 3: Task 1b: Validate the username and password against the Users collection in the MainWindow window
        private void Logon_Click(object sender, RoutedEventArgs e)
        {
            var teacher = (from Teacher t in DataSource.Teachers
                           where String.Compare(t.UserName, username.Text) == 0 //sammenligner username fra XAML til de usernames der er gemt i databasen
                           && String.Compare(t.Password, password.Password) == 0 
                           select t).FirstOrDefault(); 
            if (!String.IsNullOrEmpty(teacher.UserName) )
            {
                // Setting the session Context to the current teacher's value
                SessionContext.UserID = teacher.TeacherID;
                SessionContext.UserRole = Role.Teacher;
                SessionContext.UserName = teacher.UserName;
                SessionContext.CurrentTeacher = teacher;

                LogonSuccess(this,null);
                return;
            }
            else
            {
                var student = (from Student s in DataSource.Students
                               where String.Compare(s.UserName, username.Text) == 0
                               && String.Compare(s.Password, password.Password) == 0
                               select s).FirstOrDefault();
                if (!String.IsNullOrEmpty(student.UserName))
                {
                    // Setting the session context to the current student
                    SessionContext.UserID = student.StudentID;
                    SessionContext.UserRole = Role.Student;
                    SessionContext.UserName = student.UserName;
                    SessionContext.CurrentStudent = student;

                    LogonSuccess(this, null);
                    return;
                }

                
                

            }
            // Raising logonFailed event if none of the above conditions are fulfilled
            logonFailed(this, null);
        }
        #endregion
    }
}
